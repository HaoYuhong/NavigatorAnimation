//
//  AppDelegate.h
//  YHNavigation
//
//  Created by HaoYuhong on 2018/6/21.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

