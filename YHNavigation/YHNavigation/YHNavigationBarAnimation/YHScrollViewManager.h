//
//  YHTableViewManager.h
//  YHNavigation
//
//  Created by harry on 2018/7/10.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "YHDistanceRange.h"

@interface YHScrollViewManager : NSObject

@property (nonatomic, weak) UINavigationController *currentNav;
@property (nonatomic, assign) YHAnimateDistanceRange animateRange;
@property (nonatomic, assign) YHAnimateAlphaRange animateAlphaRange;
@property (nonatomic, weak) UIScrollView *weakScrollView;
@property (nonatomic, assign)BOOL enabledBarAnimation;

- (YHScrollViewManager *(^)(YHAnimateDistanceRange distanceRange))distanceRange;
- (YHScrollViewManager *(^)(YHAnimateAlphaRange distanceRange))alphaRange;
- (YHScrollViewManager *(^)(UIScrollView *scrollView))scrollView;

- (void)setNavigationBarHidden:(BOOL)isHidden animated:(BOOL)animated;

@end
