//
//  UIViewController+YHNavigationBarAnimation.h
//  YHNavigation
//
//  Created by harry on 2018/7/16.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHScrollViewManager.h"
@interface UIViewController (YHNavigationBarAnimation)
/**激活navagationBar 动画**/
@property (nonatomic, assign) BOOL enableBarAnimation;
/**ScrollView数据模型**/
@property (nonatomic, assign, readonly) YHScrollViewManager *manger;

- (void)enabledBarAnimationWithScrollView:(UIScrollView *)scrollView;
@end
