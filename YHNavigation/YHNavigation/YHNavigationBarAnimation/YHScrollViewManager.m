//
//  YHTableViewManager.m
//  YHNavigation
//
//  Created by harry on 2018/7/10.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import "YHScrollViewManager.h"
#import "UIView+YHParentViewController.h"
#import "YHAnimationManager.h"
@interface YHScrollViewManager()

@end

@implementation YHScrollViewManager

- (YHScrollViewManager *(^)(YHAnimateDistanceRange))distanceRange {
    return ^(YHAnimateDistanceRange distance) {
        self.animateRange = distance;
        return self;
    };
}
- (YHScrollViewManager *(^)(YHAnimateAlphaRange))alphaRange {
    return ^(YHAnimateAlphaRange alpha) {
        self.animateAlphaRange = alpha;
        return self;
    };
}

- (void)setNavigationBarHidden:(BOOL)isHidden animated:(BOOL)animated {
    if (self.currentNav) {
        [self.currentNav setNavigationBarHidden:isHidden animated:animated];
    }
}
- (YHScrollViewManager *(^)(UIScrollView *))scrollView {
    return ^(UIScrollView *scrollView) {
        [scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        return self;
    };
}
- (instancetype)init {
    if (self = [super init]) {
       
    }
    return self;
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"contentOffset:%@", change);
    NSLog(@"%@", object);
    if ([object isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)object;
        if (self.enabledBarAnimation && self.currentNav) {
            CGFloat offset = scrollView.contentOffset.y;
            if (offset >= self.animateRange.beiginLocation && offset <= self.animateRange.beiginLocation+self.animateRange.distance) {
                CGFloat tovalue = 1 - (offset - self.animateRange.distance)/ self.animateRange.distance;
                NSLog(@"fromValue:%f, toValue:%f", self.currentNav.navigationBar.alpha, tovalue);
                [YHAnimationManager alphaAnimationWithRange:YHAnimateAlphaRangeMake(self.currentNav.navigationBar.alpha, tovalue) durantion:self.animateRange.duration animView:self.currentNav.navigationBar];
            } else if (offset < self.animateRange.beiginLocation) {
                [YHAnimationManager alphaAnimationWithRange:YHAnimateAlphaRangeMake(self.currentNav.navigationBar.alpha, 1) durantion:self.animateRange.duration animView:self.currentNav.navigationBar];
            } else {
                [YHAnimationManager alphaAnimationWithRange:YHAnimateAlphaRangeMake(self.currentNav.navigationBar.alpha, 0) durantion:self.animateRange.duration animView:self.currentNav.navigationBar];
            }
        }
    }
    
}

@end
