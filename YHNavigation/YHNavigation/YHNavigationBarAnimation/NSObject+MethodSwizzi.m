//
//  NSObject+MethodSwizzi.m
//  YHNavigation
//
//  Created by harry on 2018/7/13.
//  Copyright © 2018年 Harry. All rights reserved.
//

#import "NSObject+MethodSwizzi.h"
#import <objc/runtime.h>

@implementation NSObject (MethodSwizzi)
- (void)swizzlingOriginTarget:(Class)originTarget originMethod:(SEL)originSelector swizzTarget:(Class)swizzTarget swizzMethod:(SEL)swizzSelector {
    Method originM = class_getInstanceMethod(originTarget, originSelector);
    Method targetM = class_getInstanceMethod(swizzTarget, swizzSelector);
    BOOL result = class_addMethod(originTarget, originSelector, method_getImplementation(targetM), method_getTypeEncoding(targetM));
    if (result) {
        class_replaceMethod(originTarget, originSelector, method_getImplementation(targetM), method_getTypeEncoding(targetM));
    } else {
        method_exchangeImplementations(originM, targetM);
    }
}

void myMethod(id self, IMP _cmd) {
    NSLog(@"调用了");
}
@end


