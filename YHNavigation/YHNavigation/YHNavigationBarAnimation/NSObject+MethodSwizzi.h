//
//  NSObject+MethodSwizzi.h
//  YHNavigation
//
//  Created by harry on 2018/7/13.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MethodSwizzi)
- (void)swizzlingOriginTarget:(Class)originTarget originMethod:(SEL)originMethod swizzTarget:(Class)swizzTarget swizzMethod:(SEL)swizzMethod;
@end
