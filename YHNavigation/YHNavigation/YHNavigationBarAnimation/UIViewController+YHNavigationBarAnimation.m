//
//  UIViewController+YHNavigationBarAnimation.m
//  YHNavigation
//
//  Created by harry on 2018/7/16.
//  Copyright © 2018年 Harray. All rights reserved.
//

#import "UIViewController+YHNavigationBarAnimation.h"
#import <objc/runtime.h>
#import "NSObject+MethodSwizzi.h"
#import "YHAnimationManager.h"
const char *managerkey = "Manager";
@implementation UIViewController (YHNavigationBarAnimation)
- (void)setEnableBarAnimation:(BOOL)enableBarAnimation {
    objc_setAssociatedObject(self, _cmd, @(enableBarAnimation), OBJC_ASSOCIATION_ASSIGN);
    if (enableBarAnimation) {
        self.manger.enabledBarAnimation = enableBarAnimation;
    }
}

- (BOOL)enableBarAnimation {
    return [objc_getAssociatedObject(self, @selector(setEnableBarAnimation:)) boolValue];
}
- (void)enabledBarAnimationWithScrollView:(UIScrollView *)scrollView {
    self.enableBarAnimation = YES;
    self.manger.enabledBarAnimation = YES;
    self.manger.scrollView(scrollView);
}
-(YHScrollViewManager *)manger {
    if (!objc_getAssociatedObject(self, managerkey)) {
        YHScrollViewManager *manager = [[YHScrollViewManager alloc] init];
        manager.distanceRange(YHAnimateDistanceRangeMake(100, 99, 0.25)).alphaRange(YHAnimateAlphaRangeMake(1, 0));
        manager.currentNav = self.navigationController;
        objc_setAssociatedObject(self, managerkey, manager, OBJC_ASSOCIATION_RETAIN);
    }
    return objc_getAssociatedObject(self, managerkey);
}
@end
